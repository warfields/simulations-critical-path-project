import csv
import copy

def funct(uniformDist_File, num_trials, SAN_File ) :
    #-------------------------#
    # OPEN FILES
    #-------------------------#
    global dist_file
    global f
    dist_file = uniformDist_File
    
    # Open SAN_File into array
    SAN_File_Opener = open(SAN_File, "r")

    node = []
    SAN = []
    while (True):
        aLine = SAN_File_Opener.readline()
        if aLine is "":
            SAN_File_Opener.close()
            break

        # Parse San File
        node.clear()

        node = aLine.split(' ')
        node[0] = int(node[0])
        node[1] = int(node[1])
        node[2] = float(node[2])

        #print(node)
        SAN.append(copy.deepcopy(node))
    
    #print(SAN)
    # SAN Parser
    # formated as follows: start_node end_node max_length
    # max_length needs to be multiplied by the output of the SAN_File
    # Build N[n,m] where n is nodes and m is arcs
    num_arcs  = len(SAN)
    num_nodes = 0

    for node in SAN:
        if node[1] > num_nodes:
            num_nodes = node[1]

    # Create N (node-arc incidence matrix of the network)
    N = [[0] * num_arcs for i in range(num_nodes)]

    m = 0


    for node in SAN:
        N[node[0] - 1][m] = 1
        N[node[1] - 1][m] = -1
        m += 1
    
    
    # prepopulate dictionary 
    paths = buildpathlist(N,0)
    goodpaths = []
    for path in paths:
        if path[0] is len(N) - 1:
            goodpaths.append(tuple(path[::-1]))
    #print(goodpaths)
    #print(goodpaths)
    
    dict_of_crits = {key: 0 for key in goodpaths} # contains path for key and counter for value

    rand = get_rand()
    rando_SAN = copy.deepcopy(SAN)

    #Holds concis info on nodes
    node_descriptors = {key: ([0.0] * num_nodes) for key in range(num_nodes)}

    for x in range(1,num_trials + 1):
        # Generate rando_SAN with copied values
        for x in range(len(SAN)):
            rando_SAN[x][2] = (next(rand) * SAN[x][2])
            node_descriptors[SAN[x][0] - 1][SAN[x][1] - 1] = rando_SAN[x][2]
            #print(str(SAN[x][0] - 1) + ' ' +str(node_descriptors[SAN[x][0] - 1]))

        # Call recursive algorithm
        crit = crit_pathfinder(N, rando_SAN, node_descriptors, len(N)-1)[0]
        #print (crit)

        for path in crit:
            path = tuple(path)
            if path in dict_of_crits:
                dict_of_crits[path] = dict_of_crits[path] + 1
            else:
                dict_of_crits[path] = 1

    
    # Formated as follows: 'OUTPUT:aRoot/Child,aRoot/Child,...: x.xxxx' with 
    # the 4 decimal places on the weight being very important.         
    for key in dict_of_crits:
        string_cheese = ':a1/'
        for i in key:
            i = i + 1
            if i is 1:
                continue
            string_cheese = string_cheese + str(i) + ',a' + str(i) + '/'
        string_cheese = string_cheese.rpartition(',')[0] + ':'
        print("OUTPUT ",string_cheese," {0:.4f}".format(dict_of_crits[key]/num_trials))

    global f
    f.close()

def get_rand():
    # Open UniformDist_File into Array
    global dist_file
    global f
    f = open(dist_file, "r")
    while (True):
        rand = f.readline()
        if rand is "":
            print('REACHED END OF UNIFORM DAT FILE')
            f.close()
            exit(1)
        yield float(rand)



def crit_pathfinder(N, SAN, node_descriptor, j):  # Returns ([],time)
    #  B(j) is the set of all nodes immediately before node j.
    k = 0 #  initialize index for columns of N
    l = 0 #  initialize index for predecessors to node j
    tmax = 0.0 #  initialize longest time of all paths to node j
    longest = []
    prev = 0
    

    if j is 0:
        return ([[0]], 0.0)

    for x in N[j]:
        if x is (-1):
            prev += 1

    while (l < prev):  # loop through predecessor nodes to node j

        if (N[j][k] == -1):  # if column k of N has arc entering node j

            i = 0  # begin search for predecessor node

            while (N[i][k] != 1):  # while i not a predecessor index
                i += 1  # increment i

            # /* recursive call: t is completion time of a ij*/
            temp = crit_pathfinder(N, SAN, node_descriptor, i)

            # distance from j to i
            #print(node_descriptor)
            #print(f"{j} {i}")
            dist = node_descriptor[i][j]
            #print(dist)

            t = temp[1] + dist 

            if t == tmax:
                # Handle multiple critical pathes!

                longest.extend(temp[0])

            if t > tmax: #choose largest completion time
                tmax = t
                longest = []
                longest.append(temp[0][0])

            l += 1 #/* increment predecessor index # SUSPECT CHECK FIRST

        k += 1 # increment column index

    for x in range(len(longest)):
        longest[x].extend([j])

    return (longest, tmax) # Return results

def buildpathlist(N, curr_row):
    # List of all paths in SAN
    paths = [[]]
 #   if curr_row is 0:
  #      paths = [[0]]

    for q in paths:
        q.extend([curr_row])


    lenconst1 = len(N[curr_row])
    lenconst2 = len(N)
    for link in range(lenconst1):
        #jump to new node and branch
        if N[curr_row][link] is 1:
            # Loop N[x][link] for a neg 1
            for x in range(lenconst2):
                if N[x][link] is - 1:

                    newpaths = (buildpathlist(N, x))
                    for q in newpaths:
                        q.extend([curr_row])
                    paths.extend(newpaths)
                    break
                    
    return paths
